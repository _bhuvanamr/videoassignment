import React from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import Icons from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const backgroundColor = 'transparent'

const styles = StyleSheet.create({
  playButton: {
    opacity: 0.9
  },
  playContainer: {
    flex: 1,
    backgroundColor,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  }
})

const PlayButton = props => (
  <View style={styles.playContainer}>
    <TouchableOpacity
      onPress={() => props.onPrevious()}
    >
      <Icons
        style={styles.playButton}
        name={"skip-previous"}
        color={props.theme}
        size={40}
      />
    </TouchableOpacity>
    <TouchableOpacity
      onPress={() => props.onPress()}
    >
      <Icons
        style={styles.playButton}
        name={props.paused ? 'play-circle-outline' : 'pause-circle-outline'}
        color={props.theme}
        size={65}
      />
    </TouchableOpacity>
    <TouchableOpacity
      onPress={() => props.onNext()}
    >
      <Icons
        style={styles.playButton}
        name={"skip-next"}
        color={props.theme}
        size={40}
      />
    </TouchableOpacity> 
    
   
  </View>
)

PlayButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  onPrevious: PropTypes.func.isRequired,
  onNext: PropTypes.func.isRequired,
  paused: PropTypes.bool.isRequired,
  theme: PropTypes.string.isRequired
}

export { PlayButton }
