import React, { Component } from 'react'
import { SafeAreaView, View, FlatList, StyleSheet, Text } from 'react-native';

import Video from 'react-native-af-video-player';

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

var VIDEOS = [
  {
    Videourl: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
  },
  {
    Videourl: 'http://techslides.com/demos/sample-videos/small.3gp',
  },
  {
    Videourl: 'https://file-examples.com/wp-content/uploads/2017/04/file_example_MP4_480_1_5MG.mp4',
  },
  {
    Videourl: 'http://techslides.com/demos/sample-videos/small.webm'
  },
  {
    Videourl: 'https://thepaciellogroup.github.io/AT-browser-tests/video/ElephantsDream.mp4'
  }
];


class ReactNavigationExample extends Component {
  constructor(props) {
    super(props);

    this.state = {
        currentVideo: 0, 
    };

 }



  
  onFullScreen(status) {
  }

  onMorePress() {
    // Alert.alert(
    //   'Boom',
    //   'This is an action call!',
    //   [{ text: 'Aw yeah!' }]
    // )
  }

  nextVideo() { 
    if (this.state.currentVideo != VIDEOS.length-1)
    {
        this.setState({currentVideo: this.state.currentVideo + 1}); 
    }
    else
    {
        this.setState({currentVideo: 0}); 
    }
   }
   previousVideo(){
    if (this.state.currentVideo != VIDEOS.length-1)
    {
        this.setState({currentVideo: this.state.currentVideo - 1}); 
    }
    else
    {
        this.setState({currentVideo: 0}); 
    }
   }

  render() {

    
    const logo = 'https://your-url.com/logo.png'
    const placeholder = 'https://your-url.com/placeholder.png'
    const title = 'My video title'

    return (
      <View style={styles.container}>
        <Video
          thisObj={this}
          autoPlay
          url={VIDEOS[this.state.currentVideo].Videourl}
          title={title}
          logo={logo}
          placeholder={placeholder}
          onMorePress={() => this.onMorePress()}
          onFullScreen={status => this.onFullScreen(status)}
          // fullScreenOnly = {false}
          lockPortraitOnFsExit
          rotateToFullScreen={true}
          autoPlay={true}
          // onProgress={(progress) => this.onProgress(progress)}
        />
       
      </View>
    )
  }
}

export default ReactNavigationExample
